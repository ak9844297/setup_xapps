#Git clone appmgr
git clone "https://gerrit.o-ran-sc.org/r/ric-plt/appmgr"

#Change dir to xapp_onboarder
cd appmgr/xapp_orchestrater/dev/xapp_onboarder

#If pip3 is not installed, install using the following command
yum install python3-pip

#In case dms_cli binary is already installed, it can be uninstalled using following command
pip3 uninstall xapp_onboarder -y

#Install xapp_onboarder using following command
pip3 install ./

sudo chmod 755 /usr/local/bin/dms_cli 
sudo chmod -R 755 /usr/local/lib/python3.6
sudo chmod -R 755 /usr/local/lib/python3.6