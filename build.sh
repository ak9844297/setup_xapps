cd ~/project/setup_xapps
sudo docker build -f /ric-scp-kpimon/Dockerfile xApp-registry.local:5008/qp:0.0.4 .
sudo docker build -f /ad/Dockerfile xApp-registry.local:5008/ad:0.0.2 .
sudo docker build -f /qp/Dockerfile xApp-registry.local:5008/scp-kpimon:1.0.1 .
#### onboard ####
export KONG_PROXY=`sudo kubectl get svc -n ricplt -l app.kubernetes.io/name=kong -o jsonpath='{.items[0].spec.clusterIP}'`
export APPMGR_HTTP=`sudo kubectl get svc -n ricplt --field-selector metadata.name=service-ricplt-appmgr-http -o jsonpath='{.items[0].spec.clusterIP}'`
export ONBOARDER_HTTP=`sudo kubectl get svc -n ricplt --field-selector metadata.name=service-ricplt-xapp-onboarder-http -o jsonpath='{.items[0].spec.clusterIP}'`
export CHART_REPO_URL=http://$ONBOARDER_HTTP:8080

dms_cli onboard ad/xapp-descriptor/config.json embedded-schema.json
dms_cli onboard qp/xapp-descriptor/config.json embedded-schema.json
dms_cli onboard ts/xapp-descriptor/config-file.json ts/xapp-descriptor/schema.json
dms_cli onboard ric-scp-kpimon/scp-kpimon-config-file.json embedded-schema.json

curl -L -X POST "http://$KONG_PROXY:32080/appmgr/ric/v1/xapps" --header 'Content-Type: application/json' --data-raw '{"xappName": "scp-kpimon"}'
curl -L -X POST "http://$KONG_PROXY:32080/appmgr/ric/v1/xapps" --header 'Content-Type: application/json' --data-raw '{"xappName": "trafficxapp"}'
curl -L -X POST "http://$KONG_PROXY:32080/appmgr/ric/v1/xapps" --header 'Content-Type: application/json' --data-raw '{"xappName": "ad"}'
curl -L -X POST "http://$KONG_PROXY:32080/appmgr/ric/v1/xapps" --header 'Content-Type: application/json' --data-raw '{"xappName": "qp"}'

